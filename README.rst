=====
django-niamoto-taxa
=====

django-niamoto-taxa is a reusable Django application providing models and utilities
for using and managing taxa and taxonomic trees within the niamoto system.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "niamoto_taxa" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'niamoto_taxa',
    ]

2. Run `python manage.py migrate` to create the niamoto_taxa models.

