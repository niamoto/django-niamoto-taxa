# coding: utf-8

from django.apps import AppConfig


class NiamotoTaxaConfig(AppConfig):
    name = 'niamoto_taxa'
