# coding: utf-8

from django.shortcuts import render

from niamoto_taxa.models import Taxon


def show_taxonomic_tree(request):
    return render(
        request,
        "niamoto_taxa/taxonomic_tree.html",
        {'nodes': Taxon.objects.all()},
    )
